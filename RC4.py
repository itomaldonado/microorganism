#!/usr/bin/env python
#
#       RC4, ARC4, ARCFOUR algorithm
#
#       Copyright (c) 2009 joonis new media
#       Author: Thimo Kraemer <thimo.kraemer@joonis.de>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.
#        
#        Neil Chaned to add Base64 encoding.
#        Manuel Changed to add ASCII Encoding and change array to bytearray
#

import base64
class RC4B64(object):
    def __init__(self,key=None, encoding="ASCII"):
        self.key = key
        self.__encoding = encoding
    def encrypt(self, data, key=None):
        assert key or self.key
        data = data.encode(self.__encoding)
        retData = base64.b64encode(self.__rc4crypt(data,key or self.key))
        retData = retData.decode(self.__encoding)
        return retData
    def decrypt(self, data, key=None):
        assert key or self.key
        data = data.encode(self.__encoding)
        retData = self.__rc4crypt(base64.b64decode(data),key or self.key)
        retData = retData.decode(self.__encoding)
        return retData
    
    def __rc4crypt(self, data, key):
        x = 0
        box = list(range(256))
        for i in range(256):
            x = (x + box[i] + ord(key[i % len(key)])) % 256
            box[i], box[x] = box[x], box[i]
        x = 0
        y = 0
        out = bytearray()
        for char in data:
            x = (x + 1) % 256
            y = (y + box[x]) % 256
            box[x], box[y] = box[y], box[x]
            
            origCharIntValue = char
            boxValue = box[(box[x] + box[y]) % 256]
            newCharIntValue = origCharIntValue ^ boxValue
            out.append(newCharIntValue)

        return out
    
    