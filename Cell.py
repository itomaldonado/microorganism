#!/usr/bin/env python
'''
Created on Jan 8, 2013 (Cell 0.11)

This file contains the implementation of: 
    - Cell: implementation of a "template/basic" cell.
    - decodeFunction Function: for cell to decode.
    - executeFunction Function: for cell to execute a unit of work.

Version 0.11 - Feb 12, 2013:
    - Changes:
        - Enhanced error checking on Cell object __init__ method.
        - 
        
Version 0.10 - Jan 28, 2013:
    - Changes:
        - Added the "StimuliUnit" class.
        - Added functionality to "Cell" so it can know who its parent is and who its children are.
        - Changes to the logic on "Cell", "decodeFunction" and "exeFunction".
            - see design notes below.
    - Design notes: 
        - Cell Generation information:
            - Parent's ID --> ID of the cell who created me.
            - Cell's ID --> The cell's own ID.
            - Cell Children --> List of cell's that were created by self.
                - Debating if list IDs/cell objects or just COUNT the number and not list them.
        - Decode Mechanism:
            - Grab stimuli unit from queue.
            - Construct promoter: cell Type + stimuli Type
            - Grab Gene from DNA
            - Create Executable line by using templates ("execLine = decoded_gene_template.safe_replace(dictionary)")
            - Add stimuli into exe work queue.
            - Finally, mark work unit done.
            
        - Execute Mechanism:
            - Grab work from queue,
            - execute it via exec() command
            - Finally mark work unit as done.
    
        - CellType Class:
            - Only has a "cell type" attribute it can ONLY be on DNA Base string sequence.
            - Will throw an exception if when creating the cell type object, the string provided is NOT a DNA base string sequence.
            - Will later contain the cell's generation info.
            
TODO: Add Mutation Algorithm

@author: itomaldonado
'''

import queue, concurrent.futures, re, logging, random, sys, time
from Cypher import Cypher
from DNA import DNA
from string import Template
from Utils import StimuliUnit

#########################################
########### VERSION & Logger ############
#########################################
__version_info__ = ('Cell','0','11')
__version__ = '.'.join(__version_info__)
logger = logging.getLogger("Cell")
#########################################

class Cell():
    '''
    This class is the implementation of a BASIC CELL. 
    '''
        
    def __init__(self, myDNA, cellCypher, strCellType, strCellParentID = None, stimuliQueueMaxSize = 1000, decodePoolSize = 5, exeQueueMaxSize = 1000, exePoolSize = 5):
        '''
        Initializing the cell.
        - Argument: 'myDNA' - DNA Object for copying.
        - Argument: 'cellCypher' - Cypher Object for decoding _DNA.
        - Argument: 'strCellType' - can only be strings with ONLY 'M', 'H', 'O' and 'F' characters.
        - Argument: 'strCellParentID' - Cell ID of the parent.
        - Argument: 'stimuliQueueMaxSize' int max size for the stimuli queue 
        - Argument: 'decodePoolSize' the size for the decoder pool
        - Argument: 'exeQueueMaxSize' int max size for execute queue
        - Argument: 'exePoolSize' the size for the execute pool
        - Will Raise Exception if strCellType is not formed with DNA Bases.
        - Will Raise Exception if myDNA object if not of type DNA.
        - Will Raise Exception if cellCypher object is not of type Cypher.
        - Will Raise Exception if 'decodePoolSize', 'stimuliQueueMaxSize', 'exePoolSize' or 'exeQueueMaxSize' are not of type int.
        - Will Raise Exception if 'decodePoolSize', 'stimuliQueueMaxSize', 'exePoolSize' or 'exeQueueMaxSize' are not grater than 0.
        - Will Raise Exception if strCellParentID object is not of type str.
        '''
        
        self.__baseAccepted = re.compile('[^MOFH]')
        # If the String contains characters other than allowed, raise exception.
        strCellType = strCellType.upper()
        if bool(self.__baseAccepted.search(strCellType)):
            raise Exception("Could not create CellType Object. Reason 'String <%s> contains non-DNA base characters.'" % strCellType)
        
        if not isinstance(myDNA, DNA):
            raise Exception("Could not create Cell. Reason 'Attribute <myDNA> is not of type DNA'")
        
        if not isinstance(cellCypher, Cypher):
            raise Exception("Could not create Cell. Reason 'Attribute <cellCypher> is not of type Cypher'")

        if not (isinstance(decodePoolSize, int) and isinstance(stimuliQueueMaxSize, int) and isinstance(exePoolSize, int) and isinstance(exeQueueMaxSize, int)):
            raise Exception("Could not create Cell. Reason 'A <Queue Size> Attribute was not of type int'")

        if not (decodePoolSize > 0 and stimuliQueueMaxSize > 0 and exePoolSize > 0 and exeQueueMaxSize > 0):
            raise Exception("Could not create Cell. Reason 'A <Queue Size> Attribute was not of greater or equal to 0'")

        if not( (strCellParentID is None) or (isinstance(strCellParentID, str)) ):
            raise Exception("Could not create Cell. Reason 'A <strCellParentID> Attribute was not of type str'")

        ### Attributes that will not throw Exceptions ###
        self._decodePoolSize = decodePoolSize
        self._decodePool = None
        
        self._stimuliQueueMaxSize = stimuliQueueMaxSize
        self._stimuliQueue = None
        
        self._exePoolSize = exePoolSize
        self._exeQueue = None
        
        self._exeQueueMaxSize = exeQueueMaxSize
        self._exePool = None
        
        # Generation information:
        self._cellChildren = []
        self._cellParentID = strCellParentID
        
        # Cell Needed Tools
        self._DNA = myDNA
        self._cellCypher = cellCypher
        self._alive = False
        self._shutdown = False
        
        # Cell Identification information
        self._strCellType = strCellType
        self._cellID = "CELL-" + self._strCellType + "-" + str(time.mktime(time.gmtime())) + "-" + str(random.randint(0,sys.maxsize))

        
        ### Attributes that might throw Exceptions ###
        try:
            # Queues
            self._exeQueue = queue.Queue(self._exeQueueMaxSize)
            self._stimuliQueue = queue.Queue(self._stimuliQueueMaxSize)
            
            # Cell Logger
            self.logger = logging.getLogger(self._cellID)
            
        except Exception as er:
            raise Exception("Could not create Cell. Reason '%s'" % str(er))


############################ GET METHODS ############################
    def getCellType(self):
        '''
        Method used to return the cell type.
        - Returns the String value of the '_strCellType' object.
        '''
        return self._strCellType
 
    def getCellCypher(self):
        '''
        Method to get the Cypher used by cell.
        - Returns the '_cellCypher' Object. 
        '''
        return self._cellCypher
    
    
    def getCellDNA(self):
        '''
        Method to get the Cell's DNA
        - Returns the '_DNA' Object. 
        '''
        return self._DNA
 
 
    def getStimuliQueueMaxSize(self):
        '''
        Method to get the Cell's Stimuli Queue Max's size
        - Returns the '_stimuliQueueMaxSize' Object value. 
        '''
        return self._stimuliQueueMaxSize


    def getExecQueueMaxSize(self):
        '''
        Method to get the Cell's Execute Queue Max's size
        - Returns the '_exeQueueMaxSize' Object value. 
        '''
        return self._exeQueueMaxSize
 
 
    def getDecodePoolSize(self):
        '''
        Method to get the Cell's Decode Pool Max's size
        - Returns the '_decodePoolSize' Object value. 
        '''
        return self._decodePoolSize
    
    def getExecutePoolSize(self):
        '''
        Method to get the Cell's Execute Pool Max's size
        - Returns the '_exePoolSize' Object value. 
        '''
        return self._exePoolSize    
    
    def getChildren(self):
        '''
        Method to get the Cell's children list
        - Returns the '_cellChildren' Object value. 
        '''
        return self._cellChildren
    
    def getChildrenCount(self):
        '''
        Method to get the count of the Cell's children list
        - Returns the count of '_cellChildren'.
        '''
        return len(self._cellChildren)
    
    def getCellId(self):
        '''
        Method to get the Cell's ID
        - Returns the '_cellID' Object value. 
        '''
        return self._cellID
    
    def getCellParentId(self):
        '''
        Method to get the Cell's Parent's ID
        - Returns the '_cellParentID' Object value. 
        '''
        return self._cellParentID    

#####################################################################


############################ SET METHODS ############################
    def setStimuliQueueMaxSize(self, newStimuliQueueMaxSize):
        '''
        Method to set the Cell's Stimuli Queue Max's size 
        '''
        self._stimuliQueueMaxSize = newStimuliQueueMaxSize


    def setExecQueueMaxSize(self, newExecQueueMaxSize):
        '''
        Method to set the Cell's Execute Queue Max's size
        '''
        self._exeQueueMaxSize = newExecQueueMaxSize
 
 
    def setDecodePoolSize(self, newDecodePoolSize):
        '''
        Method to set the Cell's Decode Pool Max's size 
        '''
        self._decodePoolSize = newDecodePoolSize
    
    
    def setExecutePoolSize(self, newExecPoolSize):
        '''
        Method to set the Cell's Execute Pool Max's size
        '''
        self._exePoolSize = newExecPoolSize
        
#####################################################################
        

############################ IS  METHODS ############################
    def isAlive(self):
        '''
        Method that returns the value of '_alive'.
        - Returns 'True' if _alive/started.
        - Returns 'False' otherwise.
        '''
        return self._alive
    
    
    def isShutdown(self):
        '''
        Method that returns the value of '_shutdown'.
        - Returns 'True' if shutdown process started but has not finished.
        - Returns 'False' otherwise.
        '''
        return self._shutdown
#####################################################################


############################ WORK METHODS ###########################
    
    def stimulate(self, stimuliUnit):
        '''
        Observer helper method.
        - Argument: 'stimuliUnit' Unit of work that will be used during decoding.
        '''
        self.addStimuliWorkUnit(stimuliUnit)
    
    def addStimuliWorkUnit(self, stimuliUnit):
        '''
        This method is used to add a stimuli work unit into the queue for processing.
        - Argument: 'stimuliUnit' Unit of work that will be used during decoding.
        - Will Raise Exception if Cell has not started (not alive).
        - Will Raise Exception if Cell Shutdown process has started.
        - Will Raise Exception if Stimuli queue is full.
        - Will Raise Exception if 'stimuliUnit' object is NOT instance of 'StimuliUnit' class.
        '''
        
        if ( not isinstance(stimuliUnit,StimuliUnit) ):
            raise Exception ("Stimuli not instance of StimuliUnit.")
        
        if self.isAlive() and not self.isShutdown():
            self.logger.debug("Cell ID <> Adding Stimuli to Queue: '%s'" % stimuliUnit)
            self._stimuliQueue.put(stimuliUnit, False)
            self.logger.debug("Adding Stimuli Function to Decoder Pool.")
            
            self.logger.debug("Self to String '%s'" % self)
            tempFuture = self._decodePool.submit(decodeFunction, self._stimuliQueue, self)
            #tempFuture = self._decodePool.submit(decodeFunction(stimuliUnit, self))
            
            self.logger.debug("Future Object created while submitting Stimuli Function: '%s'" % tempFuture)
        else:
            if self.isShutdown():
                raise Exception ("Stimuli cannot be added. Cell _shutdown status: '%s'" % self.isShutdown())
            else:
                raise Exception ("Stimuli cannot be added. Cell _alive status: '%s'" % self.isAlive())
                
                
    def addExeWorkUnit(self, exeWorkUnit):
        '''
        Adds a work unit to be executed.
        - Argument: 'exeWorkUnit' unit of work to be executed.
        - Will Raise Exception if Cell has not started (not alive).
        - Will Raise Exception if Cell Shutdown process has started.
        - Will Raise Exception if Execution queue is full.
        '''
        
        if (self.isAlive()) :
            self.logger.debug("Adding Execution Work to Queue: '%s'" % exeWorkUnit)
            self._exeQueue.put(exeWorkUnit, False)
            self.logger.debug("Adding Execution Function to Decoder Pool.")
            
            tempFuture = self._exePool.submit(exeFunction, self._exeQueue, self)
            #tempFuture = self._exePool.submit(exeFunction(exeWorkUnit, self))
            
            self.logger.debug("Future Object created while submitting Execution Function: '%s'" % tempFuture)
        else:
            if self.isShutdown():
                raise Exception ("Execute item cannot be added. Cell _shutdown status: '%s'" % self.isShutdown())
            else:
                raise Exception ("Execute item cannot be added. Cell _alive status: '%s'" % self.isAlive())
        

    def raiseCell(self):
        '''
        Starts the Cell Pool Processes and marks the Cell _alive.
        - Will not do anything if the cell was already _alive/started.
        '''
        if not (self.isAlive() or self.isShutdown()):
            self._exePool = concurrent.futures.ThreadPoolExecutor(max_workers=self._decodePoolSize)
            self._decodePool = concurrent.futures.ThreadPoolExecutor(max_workers=self._exePoolSize)
            #self._exePool = concurrent.futures.ProcessPoolExecutor(max_workers=self._decodePoolSize)
            #self._decodePool = concurrent.futures.ProcessPoolExecutor(max_workers=self._exePoolSize)
            self._alive = True
            
    
    def killCell(self):
        '''
        Sends the kill command to the cell.
        TODO: Kill the cell as a separate Thread.
        - Will return "True" if ('_alive' and not '_shutdown') and 'False' otherwise.
        - Will start the shutdown process waiting for all current Stimuli to finish and NOT new Stimuli.
        - Will change the status of '_alive' to 'False' once it is done.
        '''
        
        if self.isAlive() and not self.isShutdown():
            self.logger.debug("Cell Shutdown Initiated.")
            self._shutdown = True
            self._decodePool.shutdown(wait=True)
            self._alive = False
            self.logger.debug("Decoder Pool Shutdown.")
            self._exePool.shutdown(wait=True)
            self.logger.debug("Execution Pool Shutdown.")
            self._shutdown = False
            return True
        else:
            return False
#####################################################################

 
 

def decodeFunction(stimuliQueue, myCell):
    '''
    Function implementing thread. Will read off queue and Decode messages, finally queuing into execute thread-pool
    - Argument: '_stimuliQueue' Queue object with the work to be done.
    - Argument: 'myCell' the mother Cell object reference.
    
    TODO: Better implement decode/exe work! 
    TODO: Have a way to increase number of 'decoder/exe' threads

    
    Does:
        Kick-start the process to decode _DNA for the stimuli provided.
        Will decode for a Gene depending on it's type and the stimuli's type.
        
    '''
    try:
        
        objStimUnit = stimuliQueue.get()
        
        strPromoter = myCell.getCellType() + objStimUnit.getStimuliType()
        strGene = myCell.getCellDNA().copyGeneSequence(strPromoter)
        
        strToLog = "Found Gene: '%s'"
        logger.debug(strToLog % strGene)
        
        objGene = myCell.getCellCypher().deCodeCodon(strGene)
        strToLog = "Decoded Gene: '%s'"
        logger.debug( strToLog % objGene)
        
        objGene = Template(objGene)
        strExeWorkUnit = objGene.safe_substitute(objStimUnit.getStimuliDict())
        
        
        strToLog = "Work Unit to insert: '%s'"
        logger.debug( strToLog % (strExeWorkUnit) )
        
        myCell.addExeWorkUnit(strExeWorkUnit)
    except Exception as e:
            raise e
    finally:
        stimuliQueue.task_done()

def exeFunction(exeQueue, myCell):
    '''
    Function for execution. Will read off queue and execute.
    - Argument: '_exeQueue' Queue object to deposit decoded code.
    
    TODO: Better implement decode/exe work! 
    TODO: Have a way to increase number of 'decoder/exe' threads
    
    Does:
        Kick-start the process to exe the decoded Gene provided.
    '''
    try:
        workUnit = exeQueue.get()
        #workUnit=exeQueue
        
        strToLog = "To Run: '%s'"
        logger.debug(strToLog % workUnit)
        
        # Compile Code
        try:
            toExec = compile(workUnit,'<string>',"exec")
        except SyntaxError as se:
            logger.error("Syntax Error During Gene Compilation.")
            raise se
        except TypeError as te:
            logger.error("Error during Gene Compilation. Gene contains null bytes.")
            raise te
        # Execute Code
        exec(toExec, globals(), locals())
        
    except Exception as e:
        raise e
    finally:
        exeQueue.task_done()
        
            