#!/usr/bin/env python
'''
Created on Jan 30, 2013 (Utils 0.02)

This file contains the implementation of: 
    - EnvironmentStimulusQueue: implementation of the Queue handling ALL Environment Stimulus.
    - StimuliUnit Object: Simple Stimuli Unit Package.
    - Gene: Helper Class that holds a decoded Gene.
    - notifyCells Function: used by the EnvironmentStimulusQueue worker pool to notify the specific cells of stimuli.

Version 0.02 - Feb 12, 2013:
    - Changes:
        - Changed the way the stimuli pool executor handles the stimuli, instead of passing the ObserverQueue, we pass the Env Queue Reference.
            - This changes was implemented in "EnvironmentStimulusQueue.addStimulus" and "notifyCells"

Version 0.01 - Jan 30, 2013:
    - Changes:
        - Created File.
        - Created EnvironmentStimulusQueue and notifyCells.
        - Added StimuliUnit from "Cells" file because it seems more like a utility.
        
    - Design Notes:
        - EnvironmentStimulusQueue
            - Will create a worker pool.
            - Will create a list of observers.
            - Has methods to add, remove and count the observers.
            - Has a method to "addStimulus" to the worker pool.
            - To add an observer, the objObserver has to be of a type that has the method ".stimulate()".
            
        - notifyCells Function:
            - Function that will take the StimuliUnit, get the type and Stimulate the correct cell.
            
        - StimuliUnit Class:
            - Class containing two things: stimuli type and stimuli dictionary.
            - The type can ONLY be DNA base sequences.
            - The value will be read as follows:
                - *args: these are a list of values, they will be converted into dictionary s0:val0, s1:val1...
                - **kwargs: these are a list of dictionary entries that will be appended to the original, overwriting any existing entries.\
                    - Even though this functionality exists, should be used with CAUTION


@author: itomaldonado
'''

import logging, re, concurrent.futures

#########################################
########### VERSION & Logger ############
#########################################
__version_info__ = ('Utils','0','02')
__version__ = '.'.join(__version_info__)
logger = logging.getLogger("Utils")
#########################################


# Helper Methods for creating classes that validate their properties
#########################################
def getter_setter_gen(name, type_):
    def getter(self):
        return getattr(self, "__" + name)
    def setter(self, value):
        if not isinstance(value, type_):
            raise TypeError("%s attribute must be set to an instance of %s" % (name, type_))
        setattr(self, "__" + name, value)
    return property(getter, setter)

def auto_attr_check(cls):
    new_dct = {}
    for key, value in cls.__dict__.items():
        if isinstance(value, type):
            value = getter_setter_gen(key, value)
        new_dct[key] = value
    # Creates a new class, using the modified dictionary as the class dict:
    return type(cls)(cls.__name__, cls.__bases__, new_dct)
#########################################



class EnvironmentStimulusQueue:
    '''
    Queue going to be used by the cells to wait for stimulus.
    TODO: Implement shutdown.
    '''
    def __init__(self, stimulusWorkersMaxSize=10):
        '''
        Initialize the 'EnvironmentStimulusQueue' class
        - Argument: 'stimulusHandlersMaxSize' - The maximum number of workers handling stimulus.
        '''
        self.__observerList = {}
        self.__stimulusWorkersMaxSize = stimulusWorkersMaxSize
        self.__stimulusWorkerPool = concurrent.futures.ThreadPoolExecutor(max_workers=self.__stimulusWorkersMaxSize)
        self.logger = logging.getLogger("EnvironmentStimulusQueue")
    
    
    def shutdown(self):
        '''
        Stops the Environment By Stopping the Worker Pool.
        '''
        self.logger.debug("Shutdown Sequence Started.")
        self.__stimulusWorkerPool.shutdown(True)
        self.__observerList.clear()
        self.logger.debug("Shutdown Sequence Ended.")

    
    def addStimulus(self, objStimulus):
        '''
        Adds a new stimulus to the stimulus worker pool.
        - Argument: 'objStimulus' - stimulus to be added.
            - This object HAS to be from class Cell.StimuliUnit.
                - Will Raise Exception if Cell has not started (not alive).
        - Will Raise Exception if EnvironmentStimulusQueue Shutdown process has started.
        - Will Raise Exception if 'stimuliUnit' object is NOT instance of 'StimuliUnit' class.
        '''
        if ( not isinstance(objStimulus,StimuliUnit) ):
            raise Exception ("Stimuli not instance of StimuliUnit.")
        
        objFuture = self.__stimulusWorkerPool.submit(notifyCells, self, objStimulus)
        self.logger.debug("Stimulus added to Environment Stimulus Worker Pool. Future Object:'%s'" % objFuture)
    
    def addObserver(self, strObserverType, objNewObserver):
        '''
        Adds a new Observer to the Observer List.
        - Argument: 'strObserverType' - String type of stimulus to look for.
            - Has to be a DNA Base pair. 'M', 'O', 'F' and 'H' only allowed.
        - Argument: 'objNewObserver' - the new observer to be added. 
            - This object HAS to be of a class that has method ".stimulate()"
        - Will Raise Exception if 'strObserverType' is not DNA Base Pairs.
        - Will Raise Exception if 'objNewObserver' does not have the ".stimulate()" method.
        Does:
        - Will add the observer into the dictionary using the "getStimulusType()" as key.
        
        '''
        self.__baseAccepted = re.compile('[^MOFH]')
        # If the String contains characters other than allowed, raise exception.
        strObserverType = strObserverType.upper()
        
        if bool(self.__baseAccepted.search(strObserverType)):
            raise Exception("Could not add Observer. Reason 'Type <%s> contains non-DNA base characters.'" % strObserverType)

        if not hasattr(objNewObserver,"stimulate"):
            raise Exception("Could not add Observer. Reason 'Observer object does not have <stimulate> attribute/method." % strObserverType)

        self.__observerList[strObserverType] = objNewObserver
        self.logger.debug("Observer with type '%s' added." % strObserverType)


    def removeObserver(self, strObserverType):
        '''
        Will remove an observer from the Observer List.
        - Argument: 'strObserverType' - this string will be used as 'key' to remove from the Observer List.
        - Returns 'True' if Observer found and removed. False Otherwise. 
        '''
        
        try:
            self.__observerList.pop(strObserverType)
            self.logger.debug("Observer with type '%s' removed." % strObserverType)
            return True
        except KeyError:
            self.logger.warning("Observer of type: '%s' not found and removed failed. Returning False." % strObserverType)
            return False


    def observerCount(self):
        '''
        Counts the number of current observers.
        - Returns the number of observers currently applied to this queue.
        '''
        return len(self.__observerList)
    
    def getObserver(self, strStimuliType):
        '''
        Helper method to return the list of observers that match a stimuli Type.
        TODO: I want to return an array of observers!
        '''
        theObserver = self.__observerList.get(strStimuliType)
        return theObserver
    
    __iadd__ = addObserver
    __isub__ = removeObserver
    __len__  = observerCount 
    __call__ = addStimulus


class StimuliUnit():
    '''
    Class implementing a stimuli.
    This stimuli will (for now) contain only TWO THINGS:
    - stimType --> type of stimuli (DNA_ENCODED) (i.e. "print" --> "")
    - stimType --> type of stimuli (DNA_ENCODED) (i.e. "print" --> "")
    - stimValue --> List AND Dictionary of Values of stimuli (string, a char, an int....)
    '''
       
    def __init__(self, strCellType, strStimType, *args, **kwargs):
        '''
        Creates and initializes the StimuliUnit Object.
        - Argument: 'cellType' can only be strings with ONLY 'M', 'H', 'O' and 'F' characters.
        - Argument: 'stimType' can only be strings with ONLY 'M', 'H', 'O' and 'F' characters.
        - Argument: '*args' is a list of arguments that will be converted into a dictionary following:
            - s0=arg[0], s1=arg[1] ....
        - Argument: '**kwargs' is a dictionary of values that will be added to the final dictionary.
            - s0=arg[0], s1=arg[1] ........
        - Will Raise Exception if strStimType is not formed with DNA Bases.
        '''
        self.logger = logging.getLogger("StimuliUnit")
        self.__baseAccepted = re.compile('[^MOFH]')
        
        # If the strCellType argument contains characters other than allowed, raise exception.
        if bool(self.__baseAccepted.search(strCellType)):
            raise Exception("Could not create Stimuli Object. Reason 'String <%s> contains non-DNA base characters.'" % strCellType)
        self._strCellType = strStimType
        
        # If the strStimType argument contains characters other than allowed, raise exception.
        if bool(self.__baseAccepted.search(strStimType)):
            raise Exception("Could not create Stimuli Object. Reason 'String <%s> contains non-DNA base characters.'" % strStimType)
        self._strStimType = strStimType
        
        # Converting Arguments into Dictionary:
        dictStimuli = {"s"+str(i) : args[i] for i in range(0, len(args))}
        dictStimuli = dict(list(dictStimuli.items()) + list(kwargs.items()))
        self._dictStimuli = dictStimuli
        self.logger.debug("Stimuli Unit Object created: Dictionary:'%s'. Type:'%s'." % (str(self._dictStimuli), str(self._strStimType)))
            
    def getCellType(self):
        '''
        Method to return the Cell Type of the Stimuli.
        - Returns the String value of the '_strStimType' object.
        '''
        return self._strCellType
        
    def getStimuliType(self):
        '''
        Method to return the stimuli Type.
        - Returns the String value of the '_strStimType' object.
        '''
        return self._strStimType

    def getStimuliDict(self):
        '''
        Method to return the stimuli value.
        - Returns the Object contained in '_objStimValue'.
        '''
        return self._dictStimuli


@auto_attr_check
class Gene(object):
    '''
    Helper Class that holds a decoded Gene.
    '''
    cellType = str
    promoterType = str
    commandValue = str
    def __init__(self, cellType, promoterType, commandValue):
        self.cellType = cellType
        self.promoterType = promoterType
        self.commandValue = commandValue


def notifyCells(objStimulusQueue, objStimulus):
    '''
    Function to be used by EnvironmentStimulusQueue worker pool to route stimulus t the correct cells.
    - Argument: 'objStimulusQueue' - object of type 'EnvironmentStimulusQueue'
    - Argument: 'objStimulus' - object of type 'StimuliUnit'. addStimulus
    '''
    objObserver = objStimulusQueue.getObserver(objStimulus.getStimuliType())
    # TODO: this method will return an array, handle an array of observers! even if it is just ONE. Also handle 'None'.
    if objObserver is not None:
        logger.debug("Observer for Stimulus Found. Stimulating...")
        objObserver.stimulate(objStimulus)
        # TODO: if cell is "At Max Capacity" then try the Next Cell, if no more cells, put back into Queue.
    else:
        logger.warn("Stimulus Type '%s' currently unknown...")
        # TODO: If stimulus is unknown, create a new Cell to handle these.


