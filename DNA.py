#!/usr/bin/env python
'''
Created on Jan 9, 2013 (DNA 0.02)

This file will implement the DNA search/copy/read mechanism.
It will contain all classes that have to do with DNA handling.

Version 0.03 - Apr 11, 2013:
- DNA:
    - __init__: 
        - Added __dateLastModified and __strDNACache to keep track of changes and cache the DNA in memory.
    - Changed the way DNA is handled, keeping it in memory and ONLY loading it again when the "modified date" has changed.
    - Changed all variables to "private" or "__"

Version 0.02 - Jan 28, 2013:
- DNA:
    - __init__: Added "__promoterBuffer" argument for the equivalent to the "TATA" box.
    - Made sure everything get's converted into "uppercase"

@author: itomaldonado
'''
import logging, re, time, os

#########################################
########### VERSION & Logger ############
#########################################
__version_info__ = ('DNA','0','03')
__version__ = '.'.join(__version_info__)
#########################################

class DNA():
    '''
    Basic DNA Object class.
    This class will contain the DNA in memory
    and will be able to retrieve Codons or copy genes etc.
    '''
    def __init__(self, dnaFileDir, startCodon="MOFH", endCodon="HFOM", promoterBuffer="OMOMOMOMOMOMOMOM"):
        '''
        Initializes the DNA class.
        - Argument 'dnaFileDir': Will pass the dnaFileDir location
        - Argument 'startCodon': Start Codon for genes
        - Argument 'endCodon': End Codon for genes
        - Argument 'promoterBuffer': Buffer for before and after a promoter sequence.
        - Will Raise Exception if '__startCodon', '__endCodon' or '__promoterBuffer' are non-DNA base sequences.
        '''
        
        # Variables.
        self.__strDNACache = None
        self.__dateLastModified = None
        self.__baseAccepted = None
        self.__dnaFileDir = None
        self.__startCodon = None
        self.__endCodon = None
        self.__promoterBuffer = None
        self.__geneMatcherRegExPattern = None
        self.logger = logging.getLogger("DNA")
        ### 
        
        
        self.__dnaFileDir = dnaFileDir
        self.__baseAccepted = re.compile('[^MOFH]')
        self.__strDNACache = None
        self.__dateLastModified = None

        
        # If the String contains characters other than allowed, raise exception.
        startCodon = startCodon.upper()
        if bool(self.__baseAccepted.search(startCodon)):
            raise Exception("Could not create DNA Object. Reason 'String <%s> contains non-DNA base characters.'" % startCodon)
        else:
            self.__startCodon = startCodon
            
        # If the String contains characters other than allowed, raise exception.
        endCodon = endCodon.upper()
        if bool(self.__baseAccepted.search(endCodon)):
            raise Exception("Could not create DNA Object. Reason 'String <%s> contains non-DNA base characters.'" % startCodon)
        else:
            self.__endCodon = endCodon
            
        # If the String contains characters other than allowed, raise exception.
        promoterBuffer = promoterBuffer.upper()
        if bool(self.__baseAccepted.search(promoterBuffer)):
            raise Exception("Could not create DNA Object. Reason 'String <%s> contains non-DNA base characters.'" % startCodon)
        else:
            self.__promoterBuffer = promoterBuffer    
        
        
        #Create the Gene Matcher Pattern used for later recognition.
        self.__geneMatcherRegExPattern = ".*?" + self.__startCodon + "(([MOFH]{4})+?)" + self.__endCodon
        
        
        
    
    
    def copyGeneSequence(self, promoterSequence):
        '''
        Mechanism used by the cell to copy a gene by providing which promoter to search for.
        - Argument "promoterSequence" will be used to find the gene that the cell wants to copy.
        - Returns the gene base-string in a String object.
        - Will Raise an exception if there is problems reading the DNA, Finding the promoter or Reading the Gene. 
        '''
        dnaString = ""
        geneString = ""
        promoterIndex = 0
        try:
            #Getting the DNA from storage and copying it to memory.
            dnaString = self.__getDNAString()
            self.logger.debug("DNA String: '%s'" % dnaString)
            promoterSequence = promoterSequence.upper()
            
            #Adding promoter buffer sequence:
            promoterSequence = self.__promoterBuffer + promoterSequence + self.__promoterBuffer
            
            #Getting the promoter index.
            self.logger.debug("Promoter Sequence String: '%s'" % promoterSequence)
            promoterIndex = self.__findPromoter(promoterSequence, dnaString)
            self.logger.debug("Promoter Index: '%s'" % promoterIndex)
            if promoterIndex == -1:
                raise Exception("Promoter '%s' NOT found!" % promoterSequence)
            
            #Getting the Gene using RegEx
            try:
                #Substring from the promoter index.
                dnaString = dnaString[promoterIndex:]
                self.logger.debug("DNA Substring after Promoter Index: '%s'" % dnaString)
                
                #Match for a gene sequence using the promoterSequence + Gene Matcher RegEx Pattern
                matchedGeneObject = re.search(promoterSequence+self.__geneMatcherRegExPattern,dnaString)
                self.logger.debug("Matched Sequence: '%s'" % matchedGeneObject)
                
                #Get the Gene (group 1) from the match, removing the Start and End Codons.
                geneString = matchedGeneObject.group(1)
                self.logger.debug("Group #1 for Sequence: '%s'" % geneString)
                
                
            except Exception as e:
                # Convert the exception and re-raise it.
                raise Exception ("Gene could not be copied. Reason: %s" % promoterSequence)
                
        except Exception as e:
            # Log and Raise.
            self.logger.error("Error while finding promoter. Reason: %s" % e)
            raise e
        finally:
            dnaString = ""
            promoterIndex = 0
            
        return geneString
    
    def __findPromoter(self, promoterSequence, dnaString):
        '''
        Private Method to match the "promoterSequence" string with the DNA.
        - Promoter can be of length 'x' (integer) but the smaller the promoter the easier to match. 
        - Returns the first position "number/integer" of the promoter sequence.
        - Will Raise an exception if the DNA could not be loaded OR if the promoter was not found.
        '''
        try:
            promoterIndex = dnaString.find(promoterSequence.strip().upper())
            if promoterIndex == -1:
                raise Exception("Promoter NOT found!")
        except Exception as e:
            # Convert the exception and re-raise it.
            raise Exception("Error while finding promoter. Reason: %s" % e)
        
        return promoterIndex
    
    def __syncDNACache(self):
        '''
        Helper Method to refresh the DNA file from Cache *IF* modified date on file is different from what the '__dateLastModified' variable
        '''
        try:
            tempDateModified = time.gmtime((os.path.getmtime(self.__dnaFileDir)))
            # If '__dateLastModified' is None, load the modified time AND the cache.
            if self.__dateLastModified is None:
                self.logger.debug("Last Modified is 'None' loading DNA file for the first time.")
                self.__dateLastModified = tempDateModified
                dnaFileReader = open(self.__dnaFileDir, 'rU')
                self.__strDNACache = dnaFileReader.read()  
                self.logger.debug("DNA File loaded into cache.")
            elif not (self.__dateLastModified == tempDateModified):
                self.logger.debug("Cached modified date is not the same as file's modified date. Refreshing DNA cache from DNA file.")
                dnaFileReader = open(self.__dnaFileDir, 'rU')                
                tempDNA = dnaFileReader.read()  

                # Before refreshing cache, let's make sure no one else was doing it.
                if not (self.__dateLastModified == tempDateModified):
                    self.__dateLastModified = tempDateModified
                    self.__strDNACache = tempDNA 
                    self.logger.debug("DNA File loaded into cache.")
                else:
                    self.logger.debug("DNA file was not loaded. Some other thread already loaded it.")
            else:
                self.logger.debug("Cached modified date is the same as file's modified date. DNA cache is not refreshed.")

            
        except Exception as e:
            # Convert the exception and re-raise it.
            raise Exception("Error while refreshing DNA Cache. Reason: %s" % e)
        finally:
            try:
                dnaFileReader.close()
            except Exception as e:
                self.logger.warn("DNA File already closed.")

    def __readDNAFile(self, objDNAFile):
        '''
        Method that will read a DNA file into memory. If a non-base character is found, it will raise an exception.
        
        - Argument: 'objDNAFile': file object to be used while reading.
        - Will Raise Exception if non-DNA base is found.
        - Will Raise Exception if any other problem while reading.
        '''
        try:
            tempDNAString = objDNAFile.read()
            if bool(self.__baseAccepted.search(tempDNAString)):
                raise Exception("Could not create DNA Object. Reason 'DNA File contains non-DNA base characters.'")
            return tempDNAString
        except Exception as e:
            self.logger.error("Could not read from DNA File. Reason '%s'" % str(e))
            raise e
        
    def __getDNAString(self):
        '''
        Private Method to open the DNA File
        - Will use the "self.__dnaFileDir" to open the file.
        - Will return a String object with the DNA.
        - Will Raise and exception if the file cannot be loaded or read.
         
        Can be implemented differently later.
        '''
        try:
            
            dnaFileReader = open(self.__dnaFileDir, 'rU')
            DNA = dnaFileReader.read()
            
            
            self.__strDNACache = DNA
            
        except Exception as e:
            # Convert the exception and re-raise it.
            raise Exception("Error while loading DNA. Reason: %s" % e)
        finally:
            try:
                dnaFileReader.close()
            except Exception as e:
                self.logger.warn("DNA File already closed.")
        
        return self.__strDNACache
        