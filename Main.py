#!/usr/bin/env python
'''
Created on Jan 8, 2013 (itoOrganism 0.05)

Version 0.06 - Apr 16, 2013:
    - Changes:
        - Changes to make compatible with Cypher 0.02
        - Minor fixes.
        - Created two more tests, cypherTestModule and RC4TestModule.
Version 0.05 - Jan 30, 2013:
    - Changes:
        - Changes to make compatible with Cell 0.11
        - Changes to make Compatible to Utils 0.02
        - Minor fixes.
Version 0.04 - Jan 30, 2013:
    - Added environmentStimulusQueueTestModule.
    - Added a utility/helper function: 'createDNA'
Version 0.03 - Jan 28, 2013:
    - Added cellTestModule.
    - Modularized the "dnaTestModule" by removing the dnaDictionaryLoader into a different function.
    - Modularized loading/setting-up loggers.
Version 0.02 - Jan 20, 2013:
    - Added dnaTestModule.

@author: itomaldonado
'''
# Importing needed modules.
import os, errno, sys, logging, time

#########################################
########### VERSION & Logger ############
#########################################
__version_info__ = ('itoOrganism','0','06')
__version__ = '.'.join(__version_info__)
logger = logging.getLogger("Main")
#########################################



def loggingSetUp(logDir = "./dnaLog.log", logLevel = logging.DEBUG, entryFormat = logging.Formatter(fmt='[Process: \'%(process)d\' :: Thread: \'%(thread)d\'] %(asctime)s,%(msecs)d %(levelname)-5s <\'%(name)s\' :: \'%(funcName)s\'> - %(message)s', datefmt='%Y-%m-%d %H:%M:%S'), useLocalTimeZone = True):
    #########################################
    ################# LOGGER ################
    #########################################
    # Try to create the log folder, if it already exists, continue, if any other error, raise it.
    if not (logDir == sys.stdout):
        logdir = logDir
        try:     
            log_dir = os.path.dirname(logdir)
            os.makedirs(log_dir) 
        except OSError as e:     
            if e.errno != errno.EEXIST:         
                raise  
        log_file = open(logdir, 'a')
    else:
        log_file = logDir
    
    if useLocalTimeZone:
            logging.Formatter.converter = time.localtime
    else:
        logging.Formatter.converter = time.gmtime
        consoleFormatter = entryFormat
    
    loglevel = logLevel    
    consoleLogger = logging.StreamHandler(log_file)
    consoleLogger.setFormatter(consoleFormatter)
    logging.root.addHandler(consoleLogger)
    # Logging Level
    logging.root.setLevel(loglevel)

    # Test-it
    logging.debug("Logging set-up done!")
    #########################################   



def dnaTestModule(dnaCypher):
    #####################################
    ########## DNA Module Test ##########
    #####################################
    logger.info("==================== Testing DNA Module START!!!!... ====================")
    
    # Encoding Message:
    strMessage = 'print "Damian, look, it Works!!!!! Now it is time to use a newline character (\\n) now I will use a tab! {\\t} and finally a carry return [\\r] finally I will use some weird symbols inside some parenthesis (!@#$%^&*-=_+[]{}<>|,./?;:\\"). We will see if this works!"'
    logger.debug("Message: <%s> encodes to <%s>" % (strMessage,dnaCypher.enCodeCodon(strMessage)))
    
    # Variables
    promoter = "FHMOOOOFHMOOOOFHMOOOO"
    startCodon = "MOFH"
    endCodon = "HFOM"
    
    
    
    # Start Time
    startTime = int(round(time.time() * 1000))
    
    # Import DNA Class
    from DNA import DNA
     
    # Create DNA Object
    dnaObject = DNA("./cellDNA.test", startCodon, endCodon)
    strGene = dnaObject.copyGeneSequence(promoter)
    
    # Decode Gene Sequence
    strDecodedGene = dnaCypher.deCodeCodon(strGene)
    
    # Execute Code
    exec(strDecodedGene)
    
    # End Time
    endTime = int(round(time.time() * 1000))
    
    logger.info("=== The Gene Sequence is: '%s' ===" % strGene)
    logger.info("=== The Decoded Gene Sequence is: '%s' ===" % strDecodedGene)
    logger.info("Test took: '%s ms'" % (endTime - startTime))
    logger.info("==================== Testing DNA Module COMPLETE!... ====================")
    #####################################



def cellTestModule(loopTimes = 1, stimuliNumber = 1):
    #####################################
    ########## DNA Module Test ##########
    #####################################    
    logger.info("============== Testing Cell Module START. Number of Loop:%d ===============" %loopTimes)
    # imports needed for pre-processes
    from Cypher import Cypher
    from DNA import DNA
    from Cell import Cell
    from Utils import StimuliUnit
    
    # Needed Variables
    strDNADictionaryDir = "./DNA.lib"
    strCellDNADir = "./CellDNA.test"
    #dnaDictionary = dnaDictionaryLoader(strDNADictionaryDir)
    dnaCypher = Cypher(strDNADictionaryDir)
    stimuliQueueMaxSize = 1000
    decodePoolSize = 5
    exeQueueMaxSize = 1000
    exePoolSize = 5
    strCellParentID = "CELL-OHHOFHOH-1359436952.0-2054882829"
    
    # Encoding Things Needed:
    strOriginalCellType = 'IO'
    strOriginalPromoterCommand = 'PRINT'
    strOriginalCommand = "print ($s0)"
    listStimuli = ("False",)
    dictStimuli = dict(s0=True, s1=True,)
    
    # DNA Encoding the types.   
    strCellEnCodedType = dnaCypher.enCodeCodon(strOriginalCellType)
    logger.debug("strOriginalCellType: <%s> encodes to <%s>" % (strOriginalCellType,strCellEnCodedType))

    strEnCodeCommand = dnaCypher.enCodeCodon(strOriginalCommand)
    logger.debug("strEnCodeCommand: <%s> encodes to <%s>" % (strOriginalCommand,strEnCodeCommand))
    
    strEnCodedPromoterCommand = dnaCypher.enCodeCodon(strOriginalPromoterCommand)
    logger.debug("strEnCodedPromoterCommand: <%s> encodes to <%s>" % (strOriginalPromoterCommand,strEnCodedPromoterCommand))


    
    # Promoter
    strPromoterBuffer = "OMOMOMOMOMOMOMOM"
    strPromoterSequence = strPromoterBuffer + strCellEnCodedType + strEnCodedPromoterCommand + strPromoterBuffer
    logger.debug("Promoter Sequence: '%s'" % strPromoterSequence)
    
    # Actual Gene
    strStartCodon = 'MOFH'
    strEndCodon = 'HFOM'
    strPromoterBuffer = "OMOMOMOMOMOMOMOM"
    strGeneSequence = strStartCodon + strEnCodeCommand + strEndCodon
    strCompleteGene = strPromoterSequence + strGeneSequence
    logger.debug("Gene Sequence: '%s'" % strGeneSequence)
    logger.debug("Complete Gene: '%s'" % strCompleteGene)
    
    # Write Gene to File
    dnaFile = open(strCellDNADir, 'wt')
    dnaFile.write(strCompleteGene)
    dnaFile.close()
    
    # List of durations
    durationList = list()
    
    # Loop for 'loopTimes'
    for i in range(loopTimes):
        
        # To give test number accurately.
        test = i+1
        
        logger.debug("=== Test #%d STARTED. ===" % test)
        
        
        # Start Time
        startTime = int(round(time.time() * 1000))
    
        # Create DNA
        myDNA = DNA(strCellDNADir, strStartCodon, strEndCodon, strPromoterBuffer)
        
        # Create Cypher
        #myDNADictionary = dnaDictionaryLoader(strDNADictionaryDir)
        #myDNACypher = Cypher(myDNADictionary)
        myDNACypher = Cypher(strDNADictionaryDir)
        
        #Create Cell
        myCellType = strCellEnCodedType
        myCell = Cell(myDNA, myDNACypher, myCellType, strCellParentID, stimuliQueueMaxSize, decodePoolSize, exeQueueMaxSize, exePoolSize)
                      
        # Start Cell
        myCell.raiseCell()
    
        # Add Stimuli based on 'stimuliNumber'
        for j in range(stimuliNumber):
            logger.debug("Adding to Cell: Test #%d. Stimuli #%d." % (test,j))
            tempString = "'Test #%d. Stimuli #%d.'" % (test,j)
            
            #Both are the same thing
            listStimuli = (tempString,)
            dictStimuli = dict(s0=tempString)
            #Both are the same thing
            
            objTempStimuli = StimuliUnit(strEnCodedPromoterCommand, *listStimuli, **dictStimuli)
            myCell.addStimuliWorkUnit(objTempStimuli)
        
        # Print Cell Information:
        logger.info("Cell ID: '%s'" % myCell.getCellId())
        logger.info("Cell Type: '%s'" % myCell.getCellType())
        logger.info("Cell Parent ID: '%s'" % myCell.getCellParentId())
        logger.info("Cell Children Count: '%d'" % len(myCell.getChildren()))
        logger.info("Cell Children: '%s'" % myCell.getChildren())
        
        # Kill Cell
        myCell.killCell()
       
        # End Time
        endTime = int(round(time.time() * 1000))
        
        durationList.append(endTime - startTime)
        
        logger.debug("Test #%d took: '%s ms'" % (test, durationList[i]))
        logger.debug("=== Test #%d ENDED. ===" % test)
    
    stimuliCount = stimuliNumber * 1 # number of ACTUAL stimuli added to the queue.
    maxDur = max(durationList)
    minDur = min(durationList)
    avgDur = sum(durationList)/len(durationList)
    
    logger.info("=== Overall Test Status Summary: ===")
    logger.info("Num. of Tests: '%s'" % loopTimes)
    logger.info("#Stimuli/Test: '%s'" % stimuliCount) 
    logger.info("Max. Duration: '%s' ms" % maxDur)
    logger.info("Max Dur./Unit: '%s' ms" % (maxDur/stimuliCount))
    logger.info("Min. Duration: '%s' ms" % minDur)
    logger.info("Min Dur./Unit: '%s' ms" % (minDur/stimuliCount))
    logger.info("Avg. Duration: '%s' ms" % avgDur)
    logger.info("Avg Dur./Unit: '%s' ms" % (avgDur/stimuliCount))
    logger.info("====================================")
    logger.info("==================== Testing Cell Module COMPLETE!... ====================")    
    #####################################


def createDNA(dnaCypher, geneList):
    '''
    Utility Function for Testing.
    Will create an ENTIRE DNA with multiple functions.
    
    - Argument: 'geneList' should be a list/array of Utils.Gene objects
    - Returns a String with the DNA. 
    '''
    
    finalDNA = ""
    for gene in geneList:
        # DNA Encoding the types.   
        strCellEnCodedType = dnaCypher.enCodeCodon(gene.cellType)
        logger.debug("strOriginalCellType: <%s> encodes to <%s>" % (gene.cellType,strCellEnCodedType))
    
        strEnCodedPromoterCommand = dnaCypher.enCodeCodon(gene.promoterType)
        logger.debug("strEnCodedPromoterCommand: <%s> encodes to <%s>" % (gene.promoterType,strEnCodedPromoterCommand))

        strEnCodeCommand = dnaCypher.enCodeCodon(gene.commandValue)
        logger.debug("strEnCodeCommand: <%s> encodes to <%s>" % (gene.commandValue,strEnCodeCommand))
        
        # Promoter
        strPromoterBuffer = "OMOMOMOMOMOMOMOM"
        strPromoterSequence = strPromoterBuffer + strCellEnCodedType + strEnCodedPromoterCommand + strPromoterBuffer
        logger.debug("Promoter Sequence: '%s'" % strPromoterSequence)
        
        # Actual Gene
        strStartCodon = 'MOFH'
        strEndCodon = 'HFOM'
        strPromoterBuffer = "OMOMOMOMOMOMOMOM"
        strGeneSequence = strStartCodon + strEnCodeCommand + strEndCodon
        strCompleteGene = strPromoterSequence + strGeneSequence
        logger.debug("Gene Sequence: '%s'" % strGeneSequence)
        logger.debug("Complete Gene: '%s'" % strCompleteGene)
        finalDNA += strCompleteGene
    
    return finalDNA


def environmentStimulusQueueTestModule(loopTimes = 1, stimuliNumber = 1):
    #####################################
    ########## DNA Module Test ##########
    #####################################    
    logger.info("==================== Testing StimQueue Module START. Loop:%d ====================" %loopTimes)
    # imports needed for pre-processes
    from Cypher import Cypher
    from DNA import DNA
    from Cell import Cell
    from Utils import StimuliUnit, EnvironmentStimulusQueue, Gene
    
    # Needed Variables
    strDNADictionaryDir = "./DNA.lib"
    strCellDNADir = "./CellDNA.test"
    #dnaDictionary = dnaDictionaryLoader(strDNADictionaryDir)
    #dnaCypher = Cypher(dnaDictionary)
    dnaCypher = Cypher(strDNADictionaryDir)
    stimuliQueueMaxSize = 1000
    decodePoolSize = 5
    exeQueueMaxSize = 1000
    exePoolSize = 5
    stimulusWorkersMaxSize = 10
    geneList = []
    strStartCodon = 'MOFH'
    strEndCodon = 'HFOM'
    strPromoterBuffer = "OMOMOMOMOMOMOMOM"    
    
    # Add DNA for Different Cells and Same Cell, Different Promoters:
    
    # IO Cell
    strOriginalCellType = 'IO'
    strOriginalPromoterCommand = 'PRINT'
    strOriginalCommand = "print ($s0)"
    geneList.append(Gene(strOriginalCellType, strOriginalPromoterCommand, strOriginalCommand))
    strOriginalPromoterCommand = 'LOG'
    strOriginalCommand = "logging.debug($s0)"
    geneList.append(Gene(strOriginalCellType, strOriginalPromoterCommand, strOriginalCommand))
    
    # ARITHMETIC cell
    strOriginalCellType = 'ARITHMETIC'
    strOriginalPromoterCommand = 'PLUS'
    #strOriginalCommand = "print (\"PLUS: $s0 + $s1 = %s\" % str($s0 + $s1))"
    strOriginalCommand = "$s0 + $s1"
    geneList.append(Gene(strOriginalCellType, strOriginalPromoterCommand, strOriginalCommand))
    strOriginalPromoterCommand = 'MINUS'
    #strOriginalCommand = "print (\"MINUS: $s0 - $s1 = %s\" % str($s0 - $s1))"
    strOriginalCommand = "$s0 + $s1"
    geneList.append(Gene(strOriginalCellType, strOriginalPromoterCommand, strOriginalCommand))
    
    
    # Create DNA for multiple cells
    completeDNA = createDNA(dnaCypher, geneList)
    
    # Write DNA to File
    dnaFile = open(strCellDNADir, 'wt')
    dnaFile.write(completeDNA)
    dnaFile.close()
    
    # Create DNA Object
    myDNA = DNA(strCellDNADir, strStartCodon, strEndCodon, strPromoterBuffer)
    
    #Create IO Cell
    encodedCellType = dnaCypher.enCodeCodon("IO")
    myCell1_PRINT = dnaCypher.enCodeCodon('PRINT')
    myCell1_LOG = dnaCypher.enCodeCodon('LOG')
    strCellParentID = "CELL-%s-1359436952.0-2054882829" % encodedCellType
    myCell1 = Cell(myDNA, dnaCypher, encodedCellType, strCellParentID, stimuliQueueMaxSize, decodePoolSize, exeQueueMaxSize, exePoolSize)
    myCell1.raiseCell()
    
    #Create ARITHMETIC Cell
    encodedCellType = dnaCypher.enCodeCodon("ARITHMETIC")
    myCell2_PLUS = dnaCypher.enCodeCodon('PLUS')
    myCell2_MINUS = dnaCypher.enCodeCodon('MINUS')
    strCellParentID = "CELL-%s-1359436952.0-2054882829" % encodedCellType
    myCell2 = Cell(myDNA, dnaCypher, encodedCellType, strCellParentID, stimuliQueueMaxSize, decodePoolSize, exeQueueMaxSize, exePoolSize)
    myCell2.raiseCell()
    
    # List of durations
    durationList = list()
    
    # Loop for 'loopTimes'
    for i in range(loopTimes):
        
        # To give test number accurately.
        test = i+1
        
        logger.debug("=== Test #%d STARTED. ===" % test)
        
        
        # Start Time
        startTime = int(round(time.time() * 1000))
        
        # Create Environment Stimulus Queue.
        envQueue = EnvironmentStimulusQueue(stimulusWorkersMaxSize)
    
        # Add Observers:
        envQueue + (myCell1_PRINT, myCell1)
        envQueue + (myCell1_LOG, myCell1)
        
        envQueue + (myCell2_PLUS, myCell2)
        envQueue + (myCell2_MINUS, myCell2)
        
    
        # Add Stimuli based on 'stimuliNumber' for Cells.
        for j in range(stimuliNumber):
            logger.debug("Adding to Environment Stimulus Queue: Test #%d. Stimuli #%d." % (test,j))

            # myCell1 testing Global and Local Variable execution.
            #tempString = "myCell.getCellType()"
            #listStimuli = (tempString,)
            #objTempStimuli = StimuliUnit(myCell1_PRINT, *listStimuli)
            #envQueue.addStimulus(objTempStimuli)
            
            
            # myCell1 Print
            tempString = "'<Print> Test #%d. Stimuli #%d.'" % (test,j)
            listStimuli = (tempString,)
            objTempStimuli = StimuliUnit(myCell1_PRINT, *listStimuli)
            envQueue(objTempStimuli)
            
            # myCell1 Log
            tempString = "'<Log> Test #%d. Stimuli #%d.'" % (test,j)
            listStimuli = (tempString,)
            objTempStimuli = StimuliUnit(myCell1_LOG, *listStimuli)
            envQueue(objTempStimuli)
            
            # myCell2 PLUS
            listStimuli = (test,j)
            objTempStimuli = StimuliUnit(myCell2_PLUS, *listStimuli)
            envQueue(objTempStimuli)
            
            # myCell2 MINUS
            listStimuli = (test,j)
            objTempStimuli = StimuliUnit(myCell2_MINUS, *listStimuli)
            envQueue(objTempStimuli)
        
        # Shutting Down Environment Queue
        envQueue.shutdown()
                
        # End Time
        endTime = int(round(time.time() * 1000))
        durationList.append(endTime - startTime)
        
        logger.debug("Test #%d took: '%s ms'" % (test, durationList[i]))
        logger.debug("=== Test #%d ENDED. ===" % test)
    
    # Kill the Created Cells, but not skew test results, only measuring Environment Queue Performance.
    myCell1.killCell()
    myCell2.killCell()
    
    stimuliCount = stimuliNumber * 4 # number of ACTUAL stimuli added to the queue.
    maxDur = max(durationList)
    minDur = min(durationList)
    avgDur = sum(durationList)/len(durationList)
    
    logger.info("=== Overall Test Status Summary: ===")
    logger.info("Num. of Tests: '%s'" % loopTimes)
    logger.info("#Stimuli/Test: '%s'" % stimuliCount)
    logger.info("Max. Duration: '%s' ms" % maxDur)
    logger.info("Max Dur./Unit: '%s' ms" % (maxDur/stimuliCount))
    logger.info("Min. Duration: '%s' ms" % minDur)
    logger.info("Min Dur./Unit: '%s' ms" % (minDur/stimuliCount))
    logger.info("Avg. Duration: '%s' ms" % avgDur)
    logger.info("Avg Dur./Unit: '%s' ms" % (avgDur/stimuliCount))
    logger.info("====================================")
    logger.info("==================== Testing StimQueue Module COMPLETE!... ====================")    
    #####################################


def RC4TestModule(text, theKey="test"):
    '''
    Testing Module for the Encryption Module
    '''
    logger.info("=====Testing Module for the Encryption Module.======")
    try:
        from RC4 import RC4B64 # Import Module
        
        #Create a Key with the cypherKey string passed
        if len(theKey) > 2:
            salt = (ord(theKey[0]) + ord(theKey[1])) * (ord(theKey[2]))
            key=theKey + str( salt % 128 )
        else:        
            salt = ord(theKey[0])
            key=theKey + str( salt % 32 )
        
        CRYPT = RC4B64( key ) 
    except Exception as e:
        logger.fatal("Unable to import encryption utility libraries")
        raise e
    
    text2 = CRYPT.encrypt(text, CRYPT.key)
    logger.info("Hello, key: %s" % CRYPT.key)
    logger.info("Hello, encrypted this '%s' to this '%s'" % (text, CRYPT.encrypt(text, CRYPT.key)))
    logger.info("Hello, decrypted this '%s' to this '%s'" % (text2, CRYPT.decrypt(text2, CRYPT.key)))


def cypherTestModule(toWrite, file="./CypherTest.txt", toBinary=True, toEncrypted=True):
    '''
    Testing Module for the Cypher Class.
    '''
    from Cypher import Cypher # Import Module
    
    logger.info("=====Testing Module for the Cypher Class.======")
    logger.info("Writing '%s' to file '%s' with modes Binary:'%s' and Encrypted: '%s'" % (toWrite, file, str(toBinary), str(toEncrypted)))
    dnaCypher = Cypher(binary = toBinary, encrypted = toEncrypted)
    logger.info("Cypher Object Created!")
    dnaCypher.writeToFile(file, toWrite, binary = toBinary, encrypted = toEncrypted)
    logger.info("Cypher Wrote to file!")
    fileText = dnaCypher.readFromFile(file, binary = toBinary, encrypted = toEncrypted)
    logger.info("Cypher Read From file!")
    logger.info("Read from file: <%s>" % fileText)
    logger.info("Write dictionary to new file.")
    dnaCypher.dnaDictionaryWriter(dnaCypher._getDictionary(),file, binary = True, encrypted = True)
    logger.info("Dictionary written file!")
    logger.info("Switching Dictionaries!")
    dnaCypher.switchDictionary(file, binary = True, encrypted = True)
    logger.info("Switching Done!")
    logger.info("New Dictionary: <%s>" % dnaCypher._getDictionary())
    

        
if __name__=="__main__":

    ### Test Cell Module ###
    #loggingSetUp("./myCellTest.log", logging.INFO, useLocalTimeZone = False)
    #cellTestModule(loopTimes=2, stimuliNumber=50)
    
    ### Test Cell Module ###
    #loggingSetUp("./myEnvTest.log", logging.DEBUG, useLocalTimeZone = False)
    #environmentStimulusQueueTestModule(loopTimes=2, stimuliNumber=10)
    
    ### Test Cypher & Crypto! ###
    loggingSetUp(sys.stdout, logging.DEBUG, useLocalTimeZone = False)
    text = "Hello, this is Manuel Maldonado. This is testing the enCryption. (1234567890) {`~!@#$%^&*-_=+} [\\|;:'\",.<>/?]"
    RC4TestModule(text, theKey="ito")
    cypherTestModule(text, file="./CypherTest.txt", toBinary=False, toEncrypted=False)

    pass
    
    