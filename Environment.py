#!/usr/bin/env python
'''
Created on Jan 8, 2013 (Environment 0.01)

This file contains the implementation of: 
    - Cell: implementation of a "template/basic" cell.
    - CellType Object: Holds cell type and generation info.
    - StimuliUnit Object: Simple Stimuli Unit Package.
    - decodeFunction Function: for cell to decode.
    - executeFunction Function: for cell to execute a unit of work.

@author: itomaldonado
'''

import queue, concurrent.futures, re, logging
from Cypher import Cypher
from DNA import DNA
from string import Template

#########################################
########### VERSION & Logger ############
#########################################
__version_info__ = ('Environment','0','01')
__version__ = '.'.join(__version_info__)
logger = logging.getLogger("Environment")
#########################################
