#!/usr/bin/env python
'''
Created on Jan 8, 2013 (Cypher 0.01)

This class will be use to encode and decode Codons and files 
using the DNA Dictionary for Codons and the __cypherKey for files.

Any other encryption/un-encryption methods or classes needed will be implemented here.

Version 0.02 - Apr 11, 2013:
- Cypher:
    - Fixed RC4 and included it as part of Cypher class.
    - Also added more logic on creating the key
    - Added methods "writeToFile" and "readFromFile" to handle reading and writing to files.
        - These will also encrypt/decrypt data while writting/reading.
        - These will write/read in both binary and test mode. 
    - Changed all variables to "private" or "__"
    - Added the "encoding", "binary" and "encrypted" properties.
    - Added method "dnaDictionaryWriter" that will write a dictionary to a file.
    - Added method "switchDictionary" that will switch the current dictionary to a new Dictionary.
        - If an error happens, the current dictionary will be untouched.
    - Added methods called "deCodeString" and "enCodeString" for abstracting the CRYPTO library.
    - Added method "__keyGenerator" to generate keys.
    

Version 0.01 - Jan 8, 2013:
    - Created class

@author: itomaldonado
'''

import logging, csv
from io import StringIO
from RC4 import RC4B64

#########################################
########### VERSION & Logger ############
#########################################
__version_info__ = ('Cypher','0','02')
__version__ = '.'.join(__version_info__)
logger = logging.getLogger("Cypher")
#########################################

class Cypher:
    '''
    Creates the Cypher class to encode/decode Codons.
    '''
    def __init__(self, dnaDictLocation='./DNA.lib', binary=True, encrypted=True, cypherKey="itom", encoding="ASCII", codonSize=4):
        '''
        Initializes the Cypher class, 
        - Argument 'dnaDictLocation' used for decoding Codons. MUST BE provided and MUST BE a dictionary Object.
        - Argument 'binary' used to tell if dictionary file is in binary.
        - Argument 'encrypted' used to tell if the dictionary file is encrypted.
        - Argument 'codonSize' defaults to '4'.
        - Argument 'cypherKey' for Files defaults to "itom".
        - Creates the __CRYPT object used for encrypt/decrypt using the __cypherKey.
        '''
        self.__cypherKey = self.__keyGenerator(cypherKey)
        self.__CRYPT = RC4B64( key=self.__cypherKey )
        self.__binary = binary
        self.__encrypted = encrypted
        self.__encoding = encoding
        self.__codonSize = codonSize
        self.__dnaDict = dict()
        self.__dnaDictInverse = dict()
        self.__dnaDictLocation = dnaDictLocation
        self.__dnaDict = self.__dnaDictionaryLoader(self.__dnaDictLocation, binary = self.__binary, encrypted = self.__encrypted)
        # Load Dictionary and it's inverse
        for (k,v) in self.__dnaDict.items():
            self.__dnaDictInverse[v] = k
            

    def deCodeCodon(self, originalStr):
        '''
         Decodes a String Codon, return the String "meaning".
        - String length MUST BE correct size (multiple of the __codonSize)
        - Does NOT take into account Start and Stop Codons, those should be filtered before decoding.
        - Throws exception if size of Codon does not meet specs.
        '''
        inStream = StringIO(originalStr)
        outStream = StringIO()
        
        # Iterate through the buffer and look-up each codon using the library
        stopSign=True
        try:
            while stopSign:
                n1 = inStream.read(self.__codonSize)
                if len(n1) == self.__codonSize:
                    logger.debug("To be Translated: '%s'" % n1)
                    
                    # Look Up Codon
                    temp = ""
                    try:
                        temp = self.__dnaDict[n1.upper()]
                    except KeyError:
                        logger.warn("Codon '%s' does not code for anything." % n1.upper())
                        pass
                    
                    logger.debug("Translation: '%s'" % temp)
                    outStream.write(temp)
                
                elif len(n1)==0:
                    stopSign=False
                else:
                    raise Exception("Expected Codon Size is '%s' and found '%s'." % (self.__codonSize, len(n1)))
                    
            outString = outStream.getvalue()
        
        except Exception:
            raise
        finally:
            outStream.close()
            inStream.close()
        
        return outString
    
    def enCodeCodon(self, strOriginal):
        '''
         Encodes a String Codon, return the String "DNA" Coding.
        - strOriginal is the string to be comverted into DNA
        - Does NOT ADD Start and Stop Codons, those should be added after encoding.
        - Will Raise Exception if the 'Char' does not code for anything.
        '''
        inStream = StringIO(strOriginal)
        outStream = StringIO()
        
        # Iterate through the buffer and look-up each codon using the library
        stopSign=True
        try:
            while stopSign:
                n1 = inStream.read(1)
                if len(n1) == 1:
                    logger.debug("To be Translated: '%s'" % n1)
        
                    # Look Up Codon DNA Encoding
                    temp = ""
                    try:
                        temp = self.__dnaDictInverse[n1]
                    except KeyError:
                        raise Exception ("Character '%s' does not code for any Codon." % n1)
                    
                    logger.debug("Encoded to: '%s'" % temp)
                    outStream.write(temp)
                elif len(n1)==0:
                    stopSign=False
                else:
                    raise Exception("Expected Character Size is '%s' and found '%s'." % (1, len(n1)))
        
            outString = outStream.getvalue()
        
        except Exception:
            raise
        finally:
            outStream.close()
            inStream.close()
        
        return outString
    
    
    
    def writeToFile(self, fileDirectory, payLoad, binary=None, encrypted=None):
        '''
        Utilize the Cypher object to write something to a file (Whenever needed).
        It will create the file and write what was needed. WILL NOT APEND.
        Utilize bytes(tempString, "ascii") --> if you are writing on BINARY
        - Argument 'fileDirectory': Full-Qualified file name.
        - Argument 'payLoad': What to write to the file.
        - Argument 'binary': Tells if the file will be written in binary (True) or Text (False). Defaults to 'self.__binary'.
        - Argument 'encrypted': If writing to file will use the encryption mechanism. Defaults to 'self.__encrypted'.
        - Raises Exception if any error while writing.
        '''
        
        try:
            if binary is None:
                binary = self.__binary
            if encrypted is None:
                encrypted = self.__encrypted
            
            # Encrypt Payload with RC4 class.
            if encrypted:
                logger.debug("Data needs to encrypted. Encrypting...")
                payLoad = self.__enCodeString(payLoad) 
                logger.debug("Data encryption complete.") 
            
            # Choose Mode
            if binary:
                mode = 'wb'
                payLoad = payLoad.encode(self.__encoding)
            else:
                mode = 'wt'
            
            logger.debug("Writing to file in Directory: '%s'" % fileDirectory)
            logger.debug("Writing file in mode: '%s'" % mode)
            
            # Write DNA to File
            logger.debug("File opened.")
            objFile = open(fileDirectory, mode) # Writing on Text or Binary Mode.
            objFile.write(payLoad)
            logger.debug("Data writing done.")
        except Exception as e:
            logger.error("Could not write to File. Reason: %s" % str(e))
            raise e
        finally:
            try:
                objFile.close()
                logger.debug("File closed.")
            except Exception as e:
                logger.warn("File was already closed.")
    
    
    def readFromFile(self, fileDirectory, binary=None, encrypted=None):
        '''
        Utilize the Cypher object to write something to a file (Whenever needed).
        It will create the file and write what was needed. WILL NOT APEND.
        - Argument 'fileDirectory': Full-Qualified file name.
        - Argument 'binary': Tells if the file will be read in binary (True) or Text (False). Defaults to 'self.__binary'.
        - Argument 'encrypted': If reading the file will use the encryption mechanism. Defaults to 'self.__encrypted'.
        - Returns content of the file
        - Raises Exception if any error while reading.
        '''
        
        try:
            if binary is None:
                binary = self.__binary
            if encrypted is None:
                encrypted = self.__encrypted
            # Choose Mode
            if binary:
                mode = 'rb'
            else:
                mode = 'rt'
            # Write DNA to File
            logger.debug("Reading file from Directory: '%s'" % fileDirectory)
            logger.debug("Reading file in mode: '%s'" % mode)
            objFile = open(fileDirectory, mode) # Reading on Text or Binary Mode.
            logger.debug("File opened.")
            payLoad = objFile.read()
            logger.debug("File reading done.");
            
            # If Binary Flag, decode using the class' encoding.
            if binary:
                payLoad = payLoad.decode(self.__encoding)
            # Encrypt Payload with RC4 class.
            if encrypted:
                logger.debug("Data is encrypted. Decrypting...")
                payLoad = self.__deCodeString(payLoad)
                logger.debug("Decryption Complete.") 
            logger.debug("File was read. Contents: <%s>" % payLoad)
            return payLoad
        except Exception as e:
            logger.error("Could not read from File. Reason: %s" % str(e))
            raise e
        finally:
            try:
                objFile.close()
                logger.debug("File closed.")
            except Exception as e:
                logger.warn("File was already closed.")


    def dnaDictionaryWriter(self, dictObjNewDictionary,file=None, binary=None, encrypted=None):
        '''
        Writes a DNA Dictionary to a specific File.
        - Argument 'dictObjNewDictionary': Has the dict() object that contains the dictionary to be written to file.
        - Argument 'file': Destination file that the dictionary will be written to. If will truncate any existing file. Defaults to 'self.__dnaDictLocation'
        - Argument 'binary': Tells if the file will be written in binary (True) or Text (False). Defaults to 'self.__binary'.
        - Argument 'encrypted': If writing the file will use the encryption mechanism. Defaults to 'self.__encrypted'.
        - Raises Exception if any error during file writing.
        - Raises Exception if the 'dictObjNewDictionary' is not a dict() type object
        '''

        try:
            if file is None:
                file = self.__dnaDictLocation
            if binary is None:
                binary = self.__binary
            if encrypted is None:
                encrypted = self.__encrypted
        
            if not isinstance(dictObjNewDictionary, dict):
                raise Exception("dictObjNewDictionary object passed is not a 'dict' type object.")
            fileContents = "" 
            firstPass = True
            logger.debug("Writing dictionary to file: '%s'" % file)
            # Passing the dict object to a String before writing.
            try:
                for (k,v) in dictObjNewDictionary.items():
                    # For now I have to do this, since I am not reading the escaped chars correctly
                    if v == '\n':
                        v = '\\n'
                    elif v == '\r':
                        v = '\\r'
                    elif v == '\t':
                        v = '\\t'
                    elif v == ',':
                        v = '","'
                    elif v == '"':
                        v = '""""'
                    elif v == '\\':
                        v = '"\\"'

                        
                    if firstPass:
                        fileContents = k + "," + v
                        firstPass = False
                    else:
                        fileContents = fileContents + "\n" + k + "," + v
            
            except Exception as e:
                logger.error(e);
                raise e
            
            logger.debug("Dictionary Translated for Writing.")
            self.writeToFile(file, fileContents, binary, encrypted)
            logger.debug("Dictionary written to file.")
            
        except Exception as e:
            logger.error('New Dictionary could not be Written. Reason %s' % e)
            raise e
        
        
    def switchDictionary(self, newDNADictLocation, binary=None, encrypted=None):
        '''
        Changes the cypher's DNA Dictionary to a new dictionary on a specified file.
        Will return "False" and will not change IF there is an exception during new Dictionary Reading.
        - Argument 'dnaDictLocation': Destination file that the dictionary will be written to. If will truncate any existing file.
        - Argument 'binary': Tells if the file will be written in binary (True) or Text (False). Defaults to 'self.__binary'.
        - Argument 'encrypted': If writing the file will use the encryption mechanism. Defaults to 'self.__encrypted'.
        - Returns "True" if dictionary was switched "False" otherwise. If "False" is returned, no changes occurred during Switch.
        '''
        logger.debug("Dictionary switching starting...")
        temp1 = self.__dnaDict
        temp2 = self.__dnaDictInverse
        temp3 = self.__dnaDictLocation
        
        try:
            if binary is None:
                binary = self.__binary
            if encrypted is None:
                encrypted = self.__encrypted
            logger.debug("Loading new dictionary.")
            # Save in temporary storage while all reading is done.
            tempDict = self.__dnaDictionaryLoader(newDNADictLocation, binary = binary, encrypted = encrypted)
            tempInverse = dict()
            logger.debug("New dictionary loaded.")
            
            # Get the Inverse
            for (k,v) in tempDict.items():
                tempInverse[v] = k
            
            logger.debug("Inverse dictionary loaded.")
            # Finally, assign the temps to the self dictionaries.
            self.__dnaDict = tempDict
            self.__dnaDictInverse = tempInverse
            self.__dnaDictLocation = newDNADictLocation
            logger.debug("Switch Done!")
            return True
        except Exception as e:
            logger.error("Error during Dictionary Switching. Reason: '%s'" % e)
            self.__dnaDict = temp1
            self.__dnaDictInverse = temp2
            self.__dnaDictLocation = temp3
            return False 
            pass
        
        
    
    def _getDictionary(self):
        '''
        For Testing Only
        '''
        return self.__dnaDict




    def __deCodeString(self, originalStr):
        '''
         Decodes a String using the __cypherKey, return the decoded string.
        - Uses the __CRYPT to decrypt a String.
        '''  
        return self.__CRYPT.decrypt(originalStr, self.__cypherKey)
    
    def __enCodeString(self, originalStr):
        '''
         Encodes a String using the __cypherKey, return the decoded string.
        - Uses the __CRYPT to encrypt a String.
        '''  
        return self.__CRYPT.encrypt(originalStr, self.__cypherKey)


    def __dnaDictionaryLoader(self, dnaDictLocation, binary=None, encrypted=None):
        '''
        Loading DNA Dictionary from File
        - Argument 'dnaDictLocation': Tells the location of the Dictionary. Defaults to 'self.__dnaDictLocation'.
        - Argument 'binary': Tells if the file will be read in binary (True) or Text (False). Defaults to 'self.__binary'.
        - Argument 'encrypted': If reading the file will use the encryption mechanism. Defaults to 'self.__encrypted'.
        - Returns Dictionary object with the DNA dictionary
        - Raises Exception if any error during file reading.
        '''
        
        #DNA Dictionary File
        #Try to open the file
        try: 
            if binary is None:
                binary = self.__binary
            if encrypted is None:
                encrypted = self.__encrypted
                        
            logger.debug("Reading dictionary from file...")
            fileContents = self.readFromFile(dnaDictLocation, binary, encrypted)
            logger.debug("Dictionary content read from file.")
            dictBuffer = StringIO(fileContents)
            logger.debug("Buffer created.")
            dnaReader = csv.reader(dictBuffer, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL, doublequote=True, escapechar=None, skipinitialspace=False)    
            dnaDictionary = dict()
            logger.debug("CSV reader object created.")
            # Reading the DNA Dictionary File and Converting into a "dict" object.
            try:
                for (k,v) in dnaReader:
                    # For now I have to do this, since I am not reading the escaped chars correctly
                    if v == '\\n':
                        v = '\n'
                    elif v == '\\r':
                        v = '\r'
                    elif v == '\\t':
                        v = '\t'
                    dnaDictionary[k] = v
            except Exception as e:
                logger.error(e);
                raise e
            logger.debug("Dictionary Read.")
            return dnaDictionary
        except Exception as e:
            logger.error('DNA File not found or incorrectly formated. Reason %s' % e)
            raise e


    def __keyGenerator(self, cypherKey):
        '''
        Helper Method to create a Cypher Key.
        '''
        #Create a Key with the cypherKey string passed
        if len(cypherKey) > 2:
            salt = (ord(cypherKey[0]) + ord(cypherKey[1])) * (ord(cypherKey[2]))
            key=cypherKey + str( salt % 128 )
        else:        
            salt = ord(cypherKey[0])
            key=cypherKey + str( salt % 32 )
        
        return key
